function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}

function init() {
    return new Promise((resolve => {
        document.body.innerHTML = "<div  id='marketPlaceFetching'>" +
            "<table style='margin:0 auto;border-collapse: separate;\n" +
            "  border-spacing: 2px; border: 1px solid black' id='marketplaceFetcherTable'>" +
            "<tr>" +
            "<th>Time</th><th>Holz</th><th>Lehm</th><th>Eisen</th>" +
            "</tr>" +
            "</table>" +
            "</div>";

        resolve(document.getElementById("marketplaceFetcherTable"));
    }))

}

let bool = true;
let url = `https://${getCurrentWorld(location.href)}.staemme.ch/game.php?village=1326&screen=market&ajax=exchange_data`;


// let marketplaceFetching = document.getElementById("marketPlaceFetching");

async function doThis(marketplaceFetchingTable) {
    async function sleep(time) {
        return new Promise(function (resolve) {
            setTimeout(resolve, time);
        });
    }

    function createTrElementWithData(data) {
        let element = document.createElement("tr");

        console.log(data);

        data = JSON.parse(data);

        let wood = data["rates"]["wood"];
        let stone = data["rates"]["stone"];
        let iron = data["rates"]["iron"];

        let time = new Date(Date.now());
//0.964
        //0.036
        console.log(1 / wood);
        console.log(1 / stone);
        console.log(1 / iron);

        let someRandomMultiplicator = 0.964;
        let woodPrice = (1 / wood) * someRandomMultiplicator;
        let stonePrice = (1 / stone) * someRandomMultiplicator;
        let ironPrice = (1 / iron) * someRandomMultiplicator;


        element.innerHTML = "<td>" + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + "</td><td>" + woodPrice + "</td><td>" + stonePrice + "</td><td>" + ironPrice + "</td>";
        return element;
    }

    while (bool) {
        let data = httpGet(url);
        // let marketplaceFetchingTable = document.getElementById("marketPlaceFetcherTable");
        marketplaceFetchingTable.appendChild(createTrElementWithData(data));
        await sleep(10000);
    }
}


init().then((marketpalceFetchingTable) => {
    void doThis(marketpalceFetchingTable);
});







var getNewIncs = function () {
    var labels = document.querySelectorAll(".quickedit-label");
    var newIncs = [];
    for (var i = 0; i < labels.length; i++) {
        if (labels[i].innerHTML.indexOf("Angriff") !== -1) {
            newIncs.push(labels[i]);
        }
    }
    return newIncs;
};

async function sleep(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

var clickCheckox = function (incLabel) {
    var checkbox = incLabel.parentElement.parentElement.parentElement.previousElementSibling;
    checkbox.checked = true;
};

var clickBeschrift = function () {
   document.getElementsByClassName("btn")[1].click();
}

var doStuff = async function () {
    var incLabels = getNewIncs();
    await sleep(1000);
    for (var i = 0; i < incLabels.length; i++) {
        clickCheckox(incLabels[i]);
        await sleep(100);

    }
};

var program = async function () {
    await sleep(15000);
    doStuff();
    await sleep(1000);
    clickBeschrift();
};

program();


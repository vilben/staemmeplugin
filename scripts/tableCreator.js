window.onload = function () {

    var columnType = "text";

    var tableDiv = $("#tableDiv");
    tableDiv.tabulator({
        height: "311px",
        width: "750px",
        layout: "fitColumns",
        history: true,
        movableRows: true,
        persistentLayout: true,
        columns: [
            {rowHandle: true, formatter: "handle", headerSort: false, frozen: true, width: 30, minWidth: 30}
        ],
    });


// //Reset table contents on "Reset the table" button click
//     $("#reset").click(function(){
//         $("#tableDiv").tabulator("setData", tabledata);
//     });


//Add row on "Add Row" button click
    $("#addRow").click(function () {
        $("#tableDiv").tabulator("addRow", {});
    });
//Add Column
    $("#addColumn").click(function () {
        $("#tableDiv").tabulator("addColumn", {
            field: columnType,
            title: columnType,
            editableTitle: true,
            editable: true,
            editor: "input"
        });
    });
//     //undo button
//     $("#iconArrowLeft").click(function () {
//         $("#tableDiv").tabulator("undo");
//     });
//
// //redo button
//     $("#iconArrowRight").click(function () {
//         $("#tableDiv").tabulator("redo");
//     });


    //Delete row on "Delete Row" button click
    $("#iconDeleteRow").click(function () {
        var rows = $("#tableDiv").tabulator("getRows");
        rows[rows.length - 1].delete();
    });

    $("#iconDeleteColumn").click(function () {
        var columns = $("#tableDiv").tabulator("getColumns");
        console.log(columns.length);
        columns[columns.length - 1].delete();
    });


    // //Clear table on "Empty the table" button click
    // $("#iconDelete").click(function () {
    //     $("#tableDiv").tabulator("clearData");
    // });

    //trigger download of data.csv file
    $("#export").click(function () {
        var cols = $("#tableDiv").tabulator("getColumns");
        var rows = $("#tableDiv").tabulator("getRows");
        var content = createTable(rows);
        alert("count of rows: " + rows.length);

        $("#textArea").val(content);
    });

    function createTable(rows) {
        var string = "[table]";
        var cellStyle = "";

        for (var i = 0; i < rows.length; i++) {

            if (i < 1) {
                string += "[**]";
                var cells = rows[i].getCells();
                for (var c = 0; c < cells.length; c++) {
                    if (c < cells.length - 1) {
                        string += cells[c].getValue() + "[||]";
                    }
                    else {
                        string += cells[c].getValue() + "[/**]"
                    }
                }
            }
            else {
                string += "[*]";
                var cells1 = rows[i].getCells();
                for (var cc = 0; cc < cells1.length; cc++) {
                    cellStyle = cells1[cc].getColumn().getField();
                    console.log(cellStyle);
                    if (cc < cells1.length - 1) {
                        string += styleCell(cells1[cc].getValue(), cellStyle) + "[|]";
                        console.log(styleCell(cells1[cc].getValue(), cellStyle));
                    }
                    else {
                        string += styleCell(cells1[cc].getValue(), cellStyle);
                    }
                }
            }
        }
        string += "[/table]";


        return string.replace(/undefined/g, "*");
    }

    function styleCell(cellContent, cellStyle){
        var returnString = "";
        switch(cellStyle){
            case "TEXT": returnString = cellContent; break;
            case "ALLY": returnString = "[ally]"+cellContent+"[/ally]"; break;
            case "VILLAGE" : returnString = "[village]"+cellContent+"[/village]"; break;
            case "PLAYER" : returnString = "[player]"+cellContent+"[/player]"; break;
            default: returnString = cellContent; break;
        }
        return returnString;
    }

    $("#dropdownColumnType").click(function () {
        document.getElementById("myDropdown").classList.toggle("show");
    });

// Close the dropdown menu if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    };

    $("#dropdownText").click(function () {
        columnType = "TEXT";
        $("#dropdownColumnType").text("TEXT");
    });
    $("#dropdownVillage").click(function () {
        columnType = "VILLAGE";
        $("#dropdownColumnType").text("VILLAGE");
    });
    $("#dropdownAlly").click(function () {
        columnType = "ALLY";
        $("#dropdownColumnType").text("ALLY");
    });
    $("#dropdownPlayer").click(function () {
        columnType = "PLAYER";
        $("#dropdownColumnType").text("PLAYER");
    });


};
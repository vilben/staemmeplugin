async function setNames(name, sleepTime) {
    var renameIcon = document.getElementsByClassName("rename-icon");
    for (var count = 0; count < renameIcon.length; count++) {
        await rename(count, renameIcon);
        await sleep(sleepTime);
        await doRest(name);
        await sleep(sleepTime);
        await doOtherRest();
    }
}

async function rename(count, renameIcon) {
    return new Promise(resolve => {
        resolve(renameIcon[count].click());
    });
}

async function doRest(name) {
    return new Promise(resolve => {
        var quickEdit = document.getElementsByClassName("quickedit-edit")[0];
        quickEdit.getElementsByTagName("input")[0].value = name;
        resolve();
    });
}

async function doOtherRest() {
    return new Promise(resolve => {
        var okButton = document.getElementsByClassName("btn")[0];
        okButton.click();
        resolve();
    });
}

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

setNames(value, 123);
var getCurrentWorld = (url) => {
    return url.split('://')[1].split('.')[0];
}

async function sleep(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

const BUILDING_TYPES = ['main', 'barracks', 'stable', 'garage', 'church', 'snob', 'smith', 'place', 'market', 'wood', 'stone', 'iron', 'farm','storage', 'hide', 'wall'];
const BUILDING_TYPES_DISPLAY = ['main', 'barracks', 'stable', 'garage', 'smith', 'market','farm', 'mines', 'wall'];


console.log('injecting utils')

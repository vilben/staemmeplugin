window.onload = function () {
    let actualUrl;
    var urlRegex = "https(.*)=";
    var farmAssistant = "am_farm";
    var overviewVillages = "overview_villages";
    var booleanAutoIncOn = false;
    let isAutoBuild = window.localStorage.getItem('isAutoBuild') === 'true';

    let counter = 0;

    const DEFAULT_BUILD_ORDER = [
        {building: 'mines', level: 3, id: 0},
        {building: 'main', level: 3, id: 1},
        {building: 'farm', level: 3, id: 2},
        {building: 'mines', level: 10, id: 3},
        {building: 'main', level: 10, id: 4},
        {building: 'farm', level: 10, id: 5},
        {building: 'mines', level: 20, id: 6},
        {building: 'main', level: 20, id: 7},
        {building: 'farm', level: 15, id: 8},
        {building: 'smith', level: 20, id: 9},
    ]

    const currentBuildOrder = window.localStorage.getItem('buildOrder')? JSON.parse(window.localStorage.getItem('buildOrder'))?.filter(x => !!x) : DEFAULT_BUILD_ORDER;

    counter += currentBuildOrder.length;

    const buildOrderContainer = $('#buildOrderContainer');

    const createDropdown = () => {
        return $(`<label>Building<select id="building_dropdown"  class="field-style">${BUILDING_TYPES_DISPLAY.map(building => `<option>${building}</option>`)}</select></label>`);
    }
    const createLevelInput = () => {
        return $(`<label>Level<input id="level_select" class="field-style" type="number" placeholder="30"/></label>`)
    }
    const createAddButton = () => {
        return $(`<input id="add_building" type="button" value="Add"/>`);
    }
    const addButton = createAddButton();

    const createBuildOrderItem = (building, level, id) => {

        const buildingImage = `https://dsch.innogamescdn.com/asset/f874ab3b/graphic/buildings/mid/${building === 'mines' ? 'storage' : building}3.png`;

        const wrapper = $(`<div style="display:flex; flex-direction:row; justify-content: space-between; border-bottom: 1px solid black; align-content: center; align-items:center;">
                                <p>${building}</p>
                                <img style="flex-grow:0; flex-shrink:0; vertical-align: middle" width="35" height="30" src="${buildingImage}"/>
                                <p>${level}</p>
                            </div>`);
        const removeMyself = () => {
            wrapper.remove()
            const a = currentBuildOrder.filter((item) => {
                return item.id === id;
            })[0]
            delete currentBuildOrder[currentBuildOrder.indexOf(a)];
        };
        const deleter = $(`<input type="button" style="font-size: 22px;" value="&#128465;" />`);
        deleter.click(removeMyself);


        wrapper.append(deleter);
        return wrapper;
    }

    addButton.click(() => {
        const level = $('#level_select').val();
        const building = $('#building_dropdown').val();
        currentBuildOrder.push({id: counter, building: building, level: level});

        buildOrderContainer.append(createBuildOrderItem(building, level, counter));
        counter++;
    })


    $('#autoBuildContainer').append(createDropdown());
    $('#autoBuildContainer').append(createLevelInput());
    $('#autoBuildContainer').append(addButton);

    currentBuildOrder.forEach(item => buildOrderContainer.append(createBuildOrderItem(item.building, item.level, item.id)));


    document.getElementById('farmScriptA').onclick = function () {
        chrome.windows.getCurrent(function (currentWindow) {
            chrome.tabs.query({active: true, windowId: currentWindow.id}, function (activeTabs) {
                activeTabs.map(async function (tab) {
                    await checkCorrectSubPath(farmAssistant, tab);
                    chrome.tabs.executeScript(tab.id, {
                        code: 'var letter = "A";'
                    });
                    chrome.tabs.executeScript(tab.id, {file: '/scripts/farmScript.js'});
                });
            });
        });
    };

    document.getElementById('farmScriptB').onclick = function () {
        chrome.windows.getCurrent(function (currentWindow) {
            chrome.tabs.query({active: true, windowId: currentWindow.id}, function (activeTabs) {
                activeTabs.map(async function (tab) {
                    await checkCorrectSubPath(farmAssistant, tab);
                    chrome.tabs.executeScript(tab.id, {
                        code: 'var letter = "B";'
                    });
                    chrome.tabs.executeScript(tab.id, {file: '/scripts/farmScript.js'});
                });
            });
        });
    };

    document.getElementById('openTableCreator').onclick = function () {
        chrome.windows.create({
            'url': '../html/tableCreatorHtml.html',
            'type': 'popup',
            height: 750,
            width: 1250
        }, function (window) {
        });
    };
    document.getElementById('marketplaceFetcher').onclick = function () {
        chrome.windows.getCurrent(function (currentWindow) {
            chrome.tabs.query({active: true, windowId: currentWindow.id}, function (activeTabs) {
                activeTabs.map(async function (tab) {
                    chrome.tabs.executeScript(tab.id, {file: '/scripts/utils.js'});
                    chrome.tabs.executeScript(tab.id, {file: '/scripts/marketplaceFetcher.js'});
                });
            });
        });
    };

    document.getElementById('setVillageNames').onclick = function () {
        getValueAndDoScript("nameToChange", "/scripts/setVillageNames.js");
    };

    document.getElementById('timer').onclick = function () {
        document.getElementById('time').setAttribute("style", "background:green;");
        getValueAndDoScript("time", "/scripts/sendByTime.js");
    };

    document.getElementById('autoIncNameOn').onclick = async function () {
        booleanAutoIncOn = true;
        while (booleanAutoIncOn) {
            document.getElementById('autoIncNameOn').setAttribute("style", "background:green;");
            document.getElementById('autoIncNameOff').setAttribute("style", "");
            await getValueAndDoScript("autoIncNameOn", "/scripts/autoIncNamer.js");
        }
    };
    document.getElementById('autoIncNameOff').onclick = async function () {
        booleanAutoIncOn = false;
        document.getElementById('autoIncNameOff').setAttribute("style", "background:red;");
        document.getElementById('autoIncNameOn').setAttribute("style", "");
    };

    document.getElementById('startRaubzug').onclick = async function () {
        getValueAndDoScript('tbRaubzug', "/scripts/raubzugScript.js");
    };

    document.getElementById('saveBuildOrder').onclick = () => {
        console.log('saving', currentBuildOrder);
        window.localStorage.setItem('buildOrder', JSON.stringify(currentBuildOrder.filter(x => !!x)));
        console.log(window.localStorage.getItem('buildOrder'));
    }

    document.getElementById('startAutoBuild').onclick = async function (e) {
        isAutoBuild = !isAutoBuild;

        window.localStorage.setItem('isAutoBuild', isAutoBuild.toString());
        if (isAutoBuild) {
            e.target.style.backgroundColor = 'red';
            e.target.value = 'Stop!';
        } else {
            e.target.style.backgroundColor = 'lightgrey';
            e.target.value = 'Start!';
        }

        await autoBuild();
    };
    const autoBuild = async () => {
        return new Promise(function () {
            const value = JSON.stringify(currentBuildOrder);
            console.log(value);
            chrome.windows.getCurrent(function (currentWindow) {
                chrome.tabs.query({active: true, windowId: currentWindow.id}, function (activeTabs) {
                    activeTabs.map(async function (tab) {
                        await sleep(1000);
                        await checkCorrectSubPath('main', tab)
                        chrome.tabs.executeScript(tab.id, {
                            code: 'var value = ' + value+ ';'
                        });
                        await sleep(1000)
                        chrome.tabs.executeScript(tab.id, {file: '/scripts/utils.js'});
                        await sleep(1000)
                        chrome.tabs.executeScript(tab.id, {file: '/scripts/autoBuild.js'});
                    });
                });
            })
        });
    }

    chrome.tabs.onUpdated.addListener(async function (tabId, changeInfo, tab) {
        if (booleanAutoIncOn) {
            await getValueAndDoScript("autoIncNameOn", "/scripts/autoIncNamer.js");
        }
        if (isAutoBuild) {
            autoBuild();
        }
    });


    function getValueAndDoScript(id, js) {
        return new Promise(function () {
            var value = document.getElementById(id).value;
            chrome.windows.getCurrent(function (currentWindow) {
                chrome.tabs.query({active: true, windowId: currentWindow.id}, function (activeTabs) {
                    activeTabs.map(async function (tab) {
                        if (id === "nameToChange") {
                            await checkCorrectSubPath(overviewVillages, tab);
                        }
                        chrome.tabs.executeScript(tab.id, {
                            code: 'var value = "' + value + '";'
                        });
                        chrome.tabs.executeScript(tab.id, {file: js});
                    });
                });
            });
        })

    }

    async function sleep(time) {
        return new Promise(function (resolve) {
            setTimeout(resolve, time);
        });
    }

    async function checkCorrectSubPath(path, tab) {
        actualUrl = tab.url;
        if (!actualUrl.match(path)) {
            chrome.tabs.update(tab.id, {url: tab.url.match(urlRegex)[0] + path});
            await sleep(2000);
        }
    }

    if (isAutoBuild) {
        const autoBuildButton = document.getElementById('startAutoBuild');
        autoBuildButton.style.backgroundColor = 'red';
        autoBuildButton.innerText = 'Stop!';
        autoBuild();
    }
}


async function sendByTime(value) {
    let sent = false;
    let valueFromForm = value.replace(/\W+/g, "").replace(/^[a-zA-Z]*$/, "");

    while (sent !== true) {

        sent = await checkAndSend(getTime(), valueFromForm);
        await sleep(200);
    }
}

async function checkAndSend(a, b) {
    console.log(a + " " + b)
    if (a.toString() === b.toString()) {
        const sendButton = document.getElementById("troop_confirm_submit");
        console.log("click send!");
        sendButton.click();
        return true;
    }
    return false;
}

function getTime() {
    return document.getElementsByClassName("relative_time")[0].innerText.replace(/\W+/g, "").replace(/[a-zA-Z]+/, "");
}

async function sleep(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

const timerInProgressTextElement1 = document.createElement("p");
const timerInProgressTextElement2 = document.createElement("p");
const thWrap1 = document.createElement("th");
const thWrap2 = document.createElement("th");
timerInProgressTextElement1.setAttribute("style", "color:green; font-family:Impact; font-size:16px; background-color:light-green;");
timerInProgressTextElement1.innerHTML = "TIMER:";
timerInProgressTextElement2.setAttribute("style", "color:green; font-family:Impact; font-size:16px; background-color:light-green;");
timerInProgressTextElement2.innerHTML = value;
thWrap1.appendChild(timerInProgressTextElement1);
thWrap2.appendChild(timerInProgressTextElement2);

let list = document.getElementsByClassName("vis");
for (let i = 0; i < list.length; i++) {
    list[i].appendChild(thWrap1);
    list[i].appendChild(thWrap2);
}
sendByTime(value);

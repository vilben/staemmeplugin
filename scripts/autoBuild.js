const CHECK_DELAY = 10000;
const CHECK_AMOUNT_UNTIL_REFRESH = 10;


const script = (buildOrder) => {
    const allClickableButtons = Array.from(document.getElementsByClassName('btn-build'));

    const relevantButtons = allClickableButtons.map((btn) => {
        if (btn.style.display === 'none') {
            return undefined;
        }
        const buildingType = btn.getAttribute('data-building');
        const nextLevel = Number(btn.getAttribute('data-level-next'));


        if (buildingType && BUILDING_TYPES.includes(buildingType.toLowerCase())) {
            return {element: btn, buildingType, nextLevel: nextLevel};
        }
        return undefined;
    }).filter(btn => btn !== undefined);

    if (!relevantButtons.length) {
        console.log('no relevant button found');
        return false;
    }

    const decided = buildOrder.find(({building, level}) => {

        if (building === 'mines') {
            const mines = relevantButtons.filter(button => ['wood', 'stone', 'iron', 'storage'].includes(button.buildingType));
            const lowest = mines.sort((a, b) => {
                return a.nextLevel < b.nextLevel ? 1 : 0
            })[0];

            return lowest?.nextLevel <= level;
        }

        const build = relevantButtons.find(button => button.buildingType === building);

        return build?.nextLevel <= level;
    })

    let decidedButtonObject;

    if (decided.building === 'mines') {
        // find lowest mine to upgrade
        const mines = relevantButtons.filter(button => ['wood', 'stone', 'iron', 'storage'].includes(button.buildingType));
        decidedButtonObject = mines.sort((a, b) => {

            // storage always 2 levels lower than other mines
            const relevantA = a.buildingType === 'storage' ? a.nextLevel + 2 : a.nextLevel;
            const relevantB = b.buildingType === 'storage' ? b.nextLevel + 2 : b.nextLevel;

            return relevantA <= relevantB ? 0 : 1;
        })[0];
    } else {
        decidedButtonObject = relevantButtons.find(button => button.buildingType === decided.building);
    }

    if (!decided || !decidedButtonObject) {
        console.log('didnt find anything to upgrade');
        return false;
    }

    console.log('upping ' + decided.building + ' to level ' + decided.nextLevel);

    decidedButtonObject.element.click();
    return true;
}


const autoBuild = async (buildOrder, i) => {
    let hasClicked = script(buildOrder);

    if (hasClicked || i >= CHECK_AMOUNT_UNTIL_REFRESH) {
        await sleep(CHECK_DELAY * 10);
        return autoBuild(buildOrder, 0);
    }
    await sleep(CHECK_DELAY);
    return autoBuild(buildOrder, i + 1);
};
autoBuild(value, 0);

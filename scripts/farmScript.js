async function doThis() {
    var buttonsToClick = document.getElementsByClassName("farm_icon_" + letter.toLowerCase());

    async function sleep(time) {
        return new Promise(function (resolve) {
            setTimeout(resolve, time);
        });
    }

    for (var i = 0; i < buttonsToClick.length; i++) {
        await buttonsToClick[i].click();
        await sleep(205);
    }
}

doThis();

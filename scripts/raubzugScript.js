var farmWithTroops = async function (value) {
    var timer = document.getElementsByClassName("return-countdown")[0] || null;
    var timerText = null;
    if (timer != null){
        timerText = timer.innerHTML;
    }
    if (timerText == "0:00:00" || timerText == null || timerText == "" || document.getElementsByClassName("return-countdown")[0] == null) {
        console.log("schick ab!");
        await getTroops();
        await wait1sec();
        await sendTroops(value);
        await farmWithTroops();
    }
    else {
        await wait10sec();
        await farmWithTroops();
    }
};
var getTroops = async function () {
    return new Promise(async function (resolve) {
        var input = document.getElementsByClassName("unitsInput");
        await clickIt(input[0].nextElementSibling);
        await resolve(clickIt(input[1].nextElementSibling));
    });
};
var sendTroops = async function (value) {
    return new Promise(function (resolve) {
        var actionContainer = document.getElementsByClassName("action-container");
        resolve(actionContainer[value].children[0].click());
    });
};
var wait10sec = async function () {
    return new Promise(function (resolve) {
        resolve(sleep(10000));
        console.log("sleep 10 sec");
    });
};
var wait1sec = async function () {
    return new Promise(function (resolve) {
        resolve(sleep(1000));
        console.log("sleep 1 sec");
    });
};

function sleep(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    });
};

function clickIt(element) {
    return new Promise(function (resolve) {
        resolve(element.click());
    })
}

farmWithTroops(value);